import 'package:flutter_database/user.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
part 'task.jorm.dart';

class Task {
  @PrimaryKey()
  String id;
  String description;

  @BelongsTo(UserBean)
  String userId;
}

@GenBean()
class TaskBean extends Bean<Task> with _TaskBean {
  TaskBean(Adapter adapter) : super(adapter);

  final String tableName = 'Task';

  UserBean userBean;
}