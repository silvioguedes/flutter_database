import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_database/address.dart';
import 'package:flutter_database/foo.dart';
import 'package:flutter_database/task.dart';
import 'package:flutter_database/user.dart';
import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart';
import 'package:flutter_database/model.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String userName;
  String usersString;
  UserBean userBean;
  Database database;
  ModelBean modelBean;
  String databaseName = "database.db";
  String tableNameOriginal = "Model";
  String tableNameTemporary = "Model_TEMP_";
  int version = 1;

  @override
  void initState() {
    super.initState();

    init();
  }

  void migrationAddingCreateTable() async{
    // Get a location using getDatabasesPath
    var databasesPath = await getDatabasesPath();
    String pathString = path.join(databasesPath, databaseName);

    // open the database
    database = await openDatabase(pathString, version: version,
        onCreate: (Database db, int version) async {
          // When creating the db, create the table
        },
        onUpgrade: (Database db, int oldVersion, int newVersion) async{
          //when update the version, create the new table
          await db.execute('CREATE TABLE IF NOT EXISTS $tableNameTemporary (id INTEGER PRIMARY KEY, name TEXT, email TEXT)');
        });

    print("-------------------> CREATED");
  }

  void migrationAddingCopyTable(List<String> emailList) async{
    // Get the records
    List<Map> list = await database.rawQuery('SELECT * FROM $tableNameOriginal');

    for(int i=0;i<list.length;i++){
      Map<String, dynamic> map = list[i];
      List<dynamic> values = map.values.toList();

      dynamic id = values[0];
      dynamic name = values[1];
      String email = emailList[i];

      int _id = await database.rawInsert("INSERT INTO $tableNameTemporary (id, name, email) VALUES ($id, \'$name\', \'$email\')");

      print('--------------------> INSERTED $_id');
    }
  }

  void migrationRemovingCreateTable() async{
    // Get a location using getDatabasesPath
    var databasesPath = await getDatabasesPath();
    String pathString = path.join(databasesPath, databaseName);

    // open the database
    database = await openDatabase(pathString, version: version,
        onCreate: (Database db, int version) async {
          // When creating the db, create the table
        },
        onUpgrade: (Database db, int oldVersion, int newVersion) async{
          //when update the version, create the new table
          await db.execute('CREATE TABLE IF NOT EXISTS $tableNameTemporary (id INTEGER PRIMARY KEY, name TEXT)');
        });

    print("-------------------> CREATED");
  }

  void migrationRemovingCopyTable() async{
    // Get the records
    List<Map> list = await database.rawQuery('SELECT * FROM $tableNameOriginal');

    for(int i=0;i<list.length;i++){
      Map<String, dynamic> map = list[i];
      List<dynamic> values = map.values.toList();

      dynamic value1 = values[0];
      dynamic value2 = values[1];

      int _id = await database.rawInsert("INSERT INTO $tableNameTemporary (id, name) VALUES ($value1, \'$value2\')");
      print('--------------------> INSERTED $_id');
    }
  }

  void migrationUpdateTable() async{
    await database.execute('DROP TABLE IF EXISTS $tableNameOriginal');
    print('--------------------> DROPPED');

    await database.execute('ALTER TABLE $tableNameTemporary RENAME TO $tableNameOriginal');
    print('--------------------> ALTERED');

    await database.close();
    print('--------------------> CLOSED');
  }

  void initMigrationAdding() async{
    await migrationAddingCreateTable();

    await migrationAddingCopyTable(["a@a.com"]);

    await migrationUpdateTable();
  }

  void initMigrationRemoving() async{
    await migrationRemovingCreateTable();

    await migrationRemovingCopyTable();

    await migrationUpdateTable();
  }

  void init() async {
    try {
      await initMigrationAdding(); //only migrates when the table is created
    }
    catch (e) {
      //print(e);
      print("### cannot migrate ###");
    }

    //connect to a database
    var dbPath = await getDatabasesPath();

    SqfliteAdapter _adapter = new SqfliteAdapter(path.join(dbPath, databaseName));
    await _adapter.connect();

    //using Bean (DAO)
    userBean = new UserBean(_adapter);
    modelBean = new ModelBean(_adapter);

    //verifying
    bool tableExisted = await tableExists();

    //creating
    await userBean.createTable(ifNotExists: true);
    await userBean.addressBean.createTable(ifNotExists: true);
    await userBean.taskBean.createTable(ifNotExists: true);
    await modelBean.createTable(ifNotExists: true);
    await modelBean.fooBean.createTable(ifNotExists: true);

    if (!tableExisted) {
      //inserting
      await userBean.insert(new User()
        ..id = '1'
        ..name = 'One'
        ..address = (new Address()
          ..id = '1'
          ..userId = '1'
          ..street = 'Rua 1')
        ..task = (new List<Task>()
          ..add(new Task()
            ..id = '1'
            ..description = "A")..add(new Task()
            ..id = '2'
            ..description = "B")..add(new Task()
            ..id = '3'
            ..description = "C")), cascade: true);

      await userBean.insert(new User()
        ..id = '2'
        ..name = 'Two'
        ..address = (new Address()
          ..id = '2'
          ..userId = '2'
          ..street = 'Rua 2'), cascade: true);

      await userBean.insert(new User()
        ..id = '3'
        ..name = 'Tree'
        ..address = (new Address()
          ..id = '3'
          ..userId = '3'
          ..street = 'Rua 3'), cascade: true);

      Model model = new Model();
      model.id = 1;
      model.name = 'Name';
      model.foo = (new Foo()
        ..id = 1
        ..modelId = 1);

      await modelBean.insert(model, cascade: true);
    }

    await query();

    await queryAll();

    //print("-------> model.email ${(await modelBean.find(1)).email}");

    print("-------> foo.modelId ${(await modelBean.fooBean.findByModel(1)).modelId}");
  }

  Future<bool> tableExists() async{
    try {
      await userBean.getAll();
      return true;
    }
    catch(e){
      print(e);
    }

    return false;
  }

  void query() async {
    //querying
    User user = await userBean.find('1');

    setState(() {
      if(user != null) {
        userName = user.name;
      }
      else{
        userName = "";
      }
    });
  }

  void queryAll() async {
    //querying all
    List<User> users = await userBean.getAll();

    await setState(() {
      usersString = "";
    });

    for(User user in users) {
      Address address = await userBean.addressBean.findByUser(user.id);
      List<Task> taskList = await userBean.taskBean.findByUser(user.id);

      String taskString = "";
      if(!taskList.isEmpty) {
        taskString = taskString + "(";
      }

      for(Task task in taskList){
        taskString = taskString + task.description + " ";
      }

      if(!taskList.isEmpty) {
        taskString = taskString + ")";
      }

      await setState(() {
        usersString = usersString + user.name + " " + address.street + " " + taskString + ", ";
      });
    }
  }

  void drop() async {
    //dropping
    await userBean.drop();
    await userBean.addressBean.drop();
    await userBean.taskBean.drop();
  }

  void update() async {
    //updating
    try {
      User user = await userBean.find('1');
      user.name = 'New';

      await userBean.update(user);

      Address address = new Address()
        ..id = '1'
        ..street = 'Rua Nova';

      userBean.addressBean.associateUser(address, user);
      await userBean.addressBean.update(address);

      query();

      queryAll();
    }
    catch(e){
      print(e);
    }
  }

  void remove() async {
    //removing
    try {
      await userBean.remove('1');

      query();

      queryAll();
    }
    catch(e){
      print(e);
    }
  }

  void removeAll() async {
    //removing all
    try {
      await userBean.removeAll();

      query();

      queryAll();
    }
    catch(e){
      print(e);
    }
  }

  void insertLast() async{
    //inserting last
    try {
      final user = new User()
        ..id = '4'
        ..name = 'User';

      await userBean.insert(user);

      final address = new Address()
        ..id = '4'
        ..street = 'Street';

      userBean.addressBean.associateUser(address, user);
      await userBean.addressBean.insert(address);
    }
    catch(e){
      print(e);
    }

    queryAll();
  }

  void removeAddress() async{
    //removing address
    try {
      await userBean.addressBean.removeByUser('1');
    }
    catch(e){
      print(e);
    }

    queryAll();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'User name = $userName',
            ),
            Text(
              'Users = $usersString',
            ),
            RaisedButton(
              child: Text(
                  'Drop'
              ),
              onPressed: drop,
            ),
            RaisedButton(
              child: Text(
                'Update'
              ),
              onPressed: update,
            ),
            RaisedButton(
              child: Text(
                  'Remove First'
              ),
              onPressed: remove,
            ),
            RaisedButton(
              child: Text(
                  'Remove All'
              ),
              onPressed: removeAll,
            ),
            RaisedButton(
              child: Text(
                  'Insert Last'
              ),
              onPressed: insertLast,
            ),
            RaisedButton(
              child: Text(
                  'Remove Address'
              ),
              onPressed: removeAddress,
            )
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
