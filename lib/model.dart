import 'package:flutter_database/foo.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
part 'model.jorm.dart';

class Model {
  @PrimaryKey()
  int id;
  String name;

  //String email;

  @HasOne(FooBean)
  Foo foo;
}

@GenBean()
class ModelBean extends Bean<Model> with _ModelBean {
  ModelBean(Adapter adapter) :
        fooBean = new FooBean(adapter),
        super(adapter){

    fooBean.modelBean = this;
  }

  final FooBean fooBean;

  final String tableName = 'Model';
}