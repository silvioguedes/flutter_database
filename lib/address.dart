import 'package:flutter_database/user.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
part 'address.jorm.dart';

class Address {
  @PrimaryKey()
  String id;
  String street;

  @BelongsTo(UserBean)
  String userId;
}

@GenBean()
class AddressBean extends Bean<Address> with _AddressBean {
  AddressBean(Adapter adapter) : super(adapter);

  final String tableName = 'Address';

  UserBean userBean;
}