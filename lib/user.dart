import 'package:flutter_database/address.dart';
import 'package:flutter_database/task.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
part 'user.jorm.dart';

class User {
  @PrimaryKey()
  String id;
  String name;

  @HasOne(AddressBean)
  Address address;

  @HasMany(TaskBean)
  List<Task> task;
}

@GenBean()
class UserBean extends Bean<User> with _UserBean {
  UserBean(Adapter adapter) :
        addressBean = new AddressBean(adapter),
        taskBean = new TaskBean(adapter),
        super(adapter){

    addressBean.userBean = this;
    taskBean.userBean = this;
  }

  final AddressBean addressBean;
  final TaskBean taskBean;

  final String tableName = 'User';
}