import 'package:flutter_database/model.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
part 'foo.jorm.dart';

class Foo {
  @PrimaryKey()
  int id;

  @BelongsTo(ModelBean)
  int modelId;
}

@GenBean()
class FooBean extends Bean<Foo> with _FooBean {
  FooBean(Adapter adapter) : super(adapter){}

  final String tableName = 'Foo';

  ModelBean modelBean;
}